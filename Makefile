SHELL = /bin/sh

MLIPObjFiles = common/utils.cpp.o configuration.cpp.o basic_potentials.cpp.o basic_mlip.cpp.o mtpr.cpp.o \
radial_basis.cpp.o mtp.cpp.o neighborhoods.cpp.o 


MLIPFiles := $(addprefix ../mlip-v2/obj/ser/,$(MLIPObjFiles))

# ------ SETTINGS ------

CXX =		g++
CXXFLAGS =	-O3 -std=c++0x -g -pg
EXE = Main

# ------ MAKE PROCEDURE ------

all:	$(EXE)
$(EXE): $(MLIPFiles)  Main.o 
	$(CXX) $(MLIPFiles)  Main.o -o $(EXE) $(CXXFLAGS) -lgfortran

# ------ COMPILE RULES ------

Main.o: Main.cpp
	$(CXX) $(CXXFLAGS) $(ADDCFLAG) -c Main.cpp



# ------ CLEAN ------

clean:
	rm -f *.o $(EXE) *~
